# Dockerfile for test task from DR.WEB

# Main images CENTOS
FROM gcc
# Install GCC and dpkg-for building deb packages
#RUN apt -y install gcc &&\
#apt -y groupinstall "Development Tools" &&\
#apt -y install epel-release &&\
#apt repolist &&\
#apt -y install dpkg-devel dpkg-dev
# Work directory
COPY . /Skensell/epm
WORKDIR /Skensell/epm
# Compile app from source files
RUN ./configure &&\
make &&\
make install
# Build debpackages
RUN epm -vvv -f deb epm --output-dir deb-packages


